# Changelog

## 2.2.1 - 2024-11-12

### Fixed

* Pagination en nav



## 2.1.0 - 2023-06-14

### Changed

* Compatibilité SPIP 4.2
* La vérification de l'unicité des identifiants devient optionnelle et configurable
* Liste des identifiants utiles plus discrète

## 2.0.3 - 2022-02-24

### Added

* La balise `#IDENTIFIANT` attend zéro ou 2 arguments :
  * `#IDENTIFIANT` cherche un identifiant dans la boucle en cours ou en remontant la pile de boucle.
  * `#IDENTIFIANT{article, 93}` retourne l’identifiant de cet élément.

### Changed

* Compatibilité SPIP 4 minimum

### Fixed

* Corrections diverses

## 2.0.2 - 2019-11-18 (non releasée)

### Changed

* Refactoring : les identifiants ne sont plus stockés dans une table à part, mais directement dans les tables des contenus en ajoutant un champ `identifiant`.
  Ce champ n'est ajouté qu'aux tables configurées, celles possédant nativement ce champ sont ignorées.

### Added

* Pipeline `identifiants_pre_edition()`
* Fonctions :
  * `identifiants_lister_tables_identifiables()`
  * `identifiants_lister_tables_natives()`
  * `identifiants_lister_tables_utiles_manquantes()`
  * `identifiants_repertorier_tables_natives()`
  * `identifiants_adapter_tables()`
  * `identifiants_nettoyer_tables()`
  * `inc_identifiants_to_array_dist()` (Itérateur)
### Deprecated

* `identifiant_objet()`
* `identifiants_utiles()` → `identifiants_lister_utiles()`
* `action_generer_identifiant_objet_dist()` → `action_attribuer_identifiant_dist()`

### Removed

* Fonctions supprimées :
  * `maj_identifiant_objet()`
  * `tables_avec_identifiant()`
* Pipelines supprimés :
  * `formulaire_charger`
  * `formulaire_traiter`
  * `post_insertion`
  * `optimiser_base_disparus`
  * `declarer_tables_interfaces`
  * `declarer_tables_auxiliaires`